import os
import sys

import components.terminal as terminal
from components.exceptions import ScriptEndingException

__INSTALATION_TEMPLATE = '@@@ Installing %s... @@@'
__SUCCESSFUL_INSTALATION_TEMPLATE = '@@@ %s instalation was successful!'

try:
    apache = 'Apache'
    php = 'PHP'

    print(__INSTALATION_TEMPLATE % apache)
    terminal.execute_command('yum install -y httpd')
    print(__SUCCESSFUL_INSTALATION_TEMPLATE % apache)

    print(__INSTALATION_TEMPLATE % php)
    terminal.execute_command('amazon-linux-extras install -y php7.3')
    print(__SUCCESSFUL_INSTALATION_TEMPLATE % php)

    print('@@@ Initializing %s... @@@' % apache)
    terminal.execute_command("systemctl start httpd")
    print('@@@ Adding %s to boot @@@' % apache)
    enabled = terminal.execute_command("systemctl enable httpd")
    print('@@@ %s is %s @@@' % (apache, terminal.execute_command('systemctl is-enabled httpd')))

    print('@@@ Setting user permissions... @@@')
    terminal.execute_command('usermod -a -G apache ec2-user')
    terminal.execute_command('chown -R ec2-user:apache /var/www')
    terminal.execute_command('chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \\;')
    terminal.execute_command('find /var/www -type f -exec sudo chmod 0664 {} \\;')

    print('@@@ Creating test file... @@@')
    terminal.execute_command('echo \"<?php phpinfo(); ?>\" > /var/www/html/phpinfo.php')

    print('@@@ Script ended successfully! @@@')
    sys.exit(os.EX_OK)

except ScriptEndingException as sce:
    print('Error during script execution.')
    print(str(sce))
    print('Script ended.')
    sys.exit(1)
except Exception as ex:
    print('Unexpected error!')
    print(str(ex))
    sys.exit(1)
