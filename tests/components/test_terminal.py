import unittest
from pathlib import Path

import components.terminal as terminal
from components.exceptions import ScriptEndingException


class TerminalTest(unittest.TestCase):

    def setUp(self) -> None:
        self.data_folder_path = Path(__file__).parent.parent.joinpath('data')

        if not self.data_folder_path.exists():
            raise Exception('Check data folder path.')

    def test_execute_command_returns_the_output_of_a_command_if_it_succeeds(self):
        file_name = 'cat-test.txt'
        file = self.data_folder_path.joinpath(file_name)

        if not file.exists():
            self.fail('File not found.')

        with open(file) as open_file:
            expected_output = open_file.read()

        actual_output = terminal.execute_command('cat %s' % str(file.absolute()))
        self.assertEqual(expected_output, actual_output)

    def test_execute_command_raises_script_ending_exc_when_a_command_fails_and_its_message_is_the_error_message(self):
        nonexistent_file = self.data_folder_path.joinpath('file-to-break-this-test.txt')

        if nonexistent_file.exists():
            self.fail('Expected nonexistent file was found.')

        command = 'cat %s' % str(nonexistent_file.absolute())

        with self.assertRaises(ScriptEndingException) as context:
            terminal.execute_command(command)

        message = str(context.exception)
        self.assertTrue(message)
        self.assertTrue(message.isspace())
