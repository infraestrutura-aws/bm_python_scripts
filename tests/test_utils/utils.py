from pathlib import Path


def read_from_file_in_data_folder(file_path_from_within_data_folder: str) -> str:
    data_folder_path: Path = __get_data_folder_path_object()

    if not data_folder_path.exists():
        return 'Data folder does not exist. Check the path.'

    file_path = file_path_from_within_data_folder.split('/')
    file = data_folder_path.joinpath(*file_path)

    if not file.exists():
        return 'File not found.'

    with open(file) as opened_file:
        return opened_file.read()


def __get_data_folder_path_object() -> Path:
    test_folder = Path(__file__).parent.parent
    data_folder_path = test_folder.joinpath('data')
    return data_folder_path
