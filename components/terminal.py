import subprocess
from components.exceptions import ScriptEndingException


def execute_command(command):
    print('Command: %s' % command)
    try:
        completed_process = subprocess.run(command, capture_output=True, text=True, shell=True, check=True)
        return completed_process.stdout
    except subprocess.CalledProcessError as cpe:
        message = """
        Script ending error during command %s.
        Returned code: %s.
        Error message: %s
        """ % (cpe.cmd, cpe.returncode, cpe.stderr)
        raise ScriptEndingException(message)
